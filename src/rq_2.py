import matplotlib.pyplot as plt
import pandas as pd
import random

import packed_bubbles as bubbles

font_style = {'family': 'serif',
              'color': '#141414',
              'weight': 'bold',
              'size': 8,
              }


# Calculate the number of publications found for each female author
def calculate_author_occurrence(doc_type):
    data_frame = pd.read_csv('/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 02/%s_female.csv' % doc_type)
    data_frame['author_occurrence'] = data_frame.groupby('Author full names')['Author full names'].transform('size')

    data_frame.to_csv('/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 02/%s_female.csv' % doc_type, mode='w+')


# Remove duplicate occurrence of authors from CSV after calculating their occurrences
def prepare_data_to_plot(path01, path02):
    data_frame = pd.read_csv(path01, encoding='utf-8')

    # Drop redundant author names
    data_frame.drop_duplicates(subset=['author_name'], inplace=True)
    # Save CSV
    data_frame.to_csv(path02, mode='w+')

    # Return new data frame without duplicate Author names
    df_no_duplicates = pd.read_csv(path02, encoding='utf-8')
    return df_no_duplicates


# Plot bubble chart for each 'Document type'
def plot_bubble_chart(path, path_no_dup):
    df = prepare_data_to_plot(path, path_no_dup)

    # df = df[df['author_occurrence'] >= 10]

    number_of_colors = df.shape[0]
    print('Number of records', df.shape[0])

    colors = ["#" + ''.join([random.choice('56789ABCD') for j in range(6)])
              for i in range(number_of_colors)]

    bubble_chart = bubbles.BubbleChart(area=df['author_occurrence'],
                                       bubble_spacing=1)
    bubble_chart.collapse()

    fig, ax = plt.subplots(subplot_kw=dict(aspect="equal"))
    fig.set_size_inches(5, 5, forward=True)
    fig.patch.set_facecolor('#F6F5F1')

    labels = []
    for value in df['author_name'].tolist():
        occurrence = df.loc[df['author_name'] == value, 'author_occurrence'].item()
        # Adjust the minimum 'author_occurrence' i.e. the number of publications by each female author.
        if occurrence >= 3:
            labels.append(value + ", " + str(occurrence))
        else:
            labels.append("")

    bubble_chart.plot(
        ax, labels, colors, font_style)
    ax.axis("off")
    ax.relim()
    ax.autoscale_view()
    plt.show()


# Plot the distribution of publications in the dataset in a pie chart
def plot_pie_chart():
    # Create a pie chart
    labels = ['Conference Papers', 'Articles', 'Reviews', 'Editorials']
    data = [2125, 1606, 89, 230]
    plt.pie(data, labels=labels, autopct='%1.0f%%', colors=['#007575', '#00D1D1', '#F0EAE4', '#5e9494', ])
    # plt.title('Distribution of values by type')
    plt.show()


# Count the number of rows/tuples
def count_rows(path):
    df = pd.read_csv(path, encoding='utf-8')
    print('Number of records', df.shape[0])

    # df = df[df['author_occurrence'] >= 10]
    # print('Number of records greater than equal to 10', df.shape[0])
